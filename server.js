require("dotenv").config();

const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const multer = require("multer");
const DIR = "./uploads";
const app = express();
const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost:27017/mydb1", {
  useNewUrlParser: true,
  useCreateIndex: true,
  useUnifiedTopology: true,
});
const db = mongoose.connection;
db.on("error", (error) => console.error(error));
db.once("open", () => console.log("Connected to Database"));

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, DIR);
  },
  filename: (req, file, cb) => {
    cb(
      null,
      file.fieldname + "-" + Date.now() + "." + path.extname(file.originalname)
    );
  },
});
let upload = multer({ storage: storage });
app.use(function (req, res, next) {
  res.setHeader("Access-Control-Allow-Origin", "http://localhost:4200");
  res.setHeader("Access-Control-Allow-Methods", "POST");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "X-Requested-With,content-type"
  );
  res.setHeader("Access-Control-Allow-Credentials", true);
  next();
});
app.get("/api", function (req, res) {
  res.end("file upload");
});
app.post("/api/upload", upload.single("file"), function (req, res) {
  if (!req.file) {
    console.log("Your request doesn’t have any file");
    return res.send({
      success: false,
    });
  } else {
    console.log("Your file has been received successfully");
    return res.send({
      success: true,
    });
  }
});

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use("/uploads", express.static("uploads"));

app.use(express.static(path.join(__dirname, "public")));
const apiRouter = require("./src/routes/api");
app.use("/api/v1", apiRouter);
app.listen(3000, () => console.log("server started"));
