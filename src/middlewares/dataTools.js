const fs = require("fs");

// filtering
exports.paginate = async function (page, limit, filter, sort, model, populate) {
  try {
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const results = {};
    results.meta = {};
    const total = await model.countDocuments().exec();
    results.meta.Total_Data_in_database = total;
    if (endIndex < total) {
      results.meta.next = {
        page: page + 1,
        limit: limit,
      };
    }
    if (startIndex > 0) {
      results.meta.previous = {
        page: page - 1,
        limit: limit,
      };
    }
    // ** partial search
    if (filter) {
      samp = [filter];
      // const searchterms = Object.entries(samp);
      // const query = searchterms.map(function(parti) {
      //   const reg = new RegExp(parti[1], 'i');
      //   return { [parti[0]]: reg };
      // });
      partial = { $and: samp };
    }
    if (!filter) partial = filter;
    // ***
    results.data = await model
      .find(partial)
      .populate(populate)
      .sort(sort)
      .limit(limit)
      .skip(startIndex)
      .exec();
    results.status = "success";
    return results;
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
};

//partial search

exports.partialsearch = async function (
  page,
  limit,
  search,
  sort,
  model,
  populate
) {
  try {
    const startIndex = (page - 1) * limit;
    const endIndex = page * limit;
    const results = {};
    results.meta = {};
    const total = await model.countDocuments().exec();
    results.meta.Total_Data_in_database = total;
    if (endIndex < total) {
      results.meta.next = {
        page: page + 1,
        limit: limit,
      };
    }
    if (startIndex > 0) {
      results.meta.previous = {
        page: page - 1,
        limit: limit,
      };
    }
    // ** partial search
    if (search) {
      samp = search;
      const searchterms = Object.entries(samp);
      const query = searchterms.map(function (parti) {
        const reg = new RegExp(parti[1], "i");
        return { [parti[0]]: reg };
      });
      partial = { $and: query };
    }
    if (!search) partial = search;
    // ***
    results.data = await model
      .find(partial)
      .populate(populate)
      .sort(sort)
      .limit(limit)
      .skip(startIndex)
      .exec();
    results.status = "success";
    return results;
  } catch (e) {
    res.status(400).json({ message: e.message });
  }
};
//delete images/files from uploads folder

exports.deleteimage = async function (path) {
  try {
    fs.unlink(path, function (err) {
      if (err) console.log(err);
      // if no error, file has been deleted successfully
      // console.log('File deleted!');
    });
  } catch (err) {
    throw Error("Error while delteing image from server  ");
  }
};
