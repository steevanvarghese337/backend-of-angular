const express = require("express");
const router = express.Router();

const reviewController = require("../../controllers/review.controller");

// get all data
router.get("/all", reviewController.list);
// get data using id
router.get("/:id", reviewController.show);
// post data
router.post("/create", reviewController.create);
// update data using id
router.put("/update/:id", reviewController.update);
// delete data using id
router.delete("/:id", reviewController.delete);

module.exports = router;
