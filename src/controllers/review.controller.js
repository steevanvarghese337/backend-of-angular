const reviewService = require("../services/reviewService");
const { createValidation } = require("../validations/schema/review.schema");
const { updateValidation } = require("../validations/schema/review.schema");

exports.list = async function (req, res, next) {
  try {
    const review = await reviewService.list({}, (page = 10), (limit = 10));
    return res.status(200).json({ data: review });
  } catch (e) {
    return res.status(400).json({ status: 400, message: e.message });
  }
};
// api for getting data using id

exports.show = async function (req, res, next) {
  const id = req.params.id;

  try {
    const review = await reviewService.show(id);
    return res.status(200).json({ data: review });
  } catch (e) {
    return res.status(400).json({ status: 400, message: e.message });
  }
};

// api for posting
exports.create = async function (req, res, next) {
  // Lets Validate the data before a user register
  const { review } = req.body;

  try {
    const { error } = await createValidation(review);

    if (error) return res.status(422).send(error.details);
    dat = await reviewService.create(review);
    return res.status(200).json({ data: dat });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

// api for update
exports.update = async function (req, res, next) {
  try {
    const id = req.params.id;
    const { review } = req.body;
    const { error } = await updateValidation(review);

    if (error) return res.status(422).send(error.details);
    data = await reviewService.update(id, review);
    return res.status(200).json({ data: data });
  } catch (e) {
    return res.status(400).json({ status: 400, message: e.message });
  }
};
// api for delete
exports.delete = async function (req, res, next) {
  const id = req.params.id;

  try {
    const data = await reviewService.delete(id);

    return res.status(204).json({ data: data });
  } catch (e) {
    return res.status(400).json({ status: 400, message: e.message });
  }
};
