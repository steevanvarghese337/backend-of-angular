const { any } = require("@hapi/joi");
const mongoose = require("mongoose");

const Review = mongoose.model("review", {
  name: { type: String, required: false },
  mobile: { type: Number, required: false },
  email: { type: String, required: false },
  anonymous: { type: String, required: false },
  pictma: { type: String, required: false },
  location: { type: String, required: false },
  tags: { type: String, required: false },
  attachments: { type: String, required: false },
  content: { type: String, required: false },
});
module.exports = { Review };
