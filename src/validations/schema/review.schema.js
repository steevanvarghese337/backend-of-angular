const Joi = require("@hapi/joi");
const createValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().required(),
    mobile: Joi.number().required(),
    email: Joi.string().optional(),
    anonymous: Joi.string().optional(),
    pictma: Joi.string().optional(),
    tags: Joi.string().optional(),
    location: Joi.string().optional(),
    attachments: Joi.array().optional(),
    content: Joi.string().optional(),
  });
  return schema.validate(data);
};
const updateValidation = (data) => {
  const schema = Joi.object({
    name: Joi.string().optional(),
    mobile: Joi.number().optional(),
    email: Joi.string().optional(),
    anonymous: Joi.string().optional(),
    pictma: Joi.string().optional(),
    tags: Joi.string().optional(),
    location: Joi.string().optional(),
    attachments: Joi.array().optional(),
    content: Joi.string().optional(),
  });
  return schema.validate(data);
};
module.exports.createValidation = createValidation;
module.exports.updateValidation = updateValidation;
