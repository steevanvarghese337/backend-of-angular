const { Review } = require("../models/review.model");
const pagination = require("../middlewares/datatools");

// service for get all  data
exports.list = async function (res) {
  try {
    const review = await Review.find({});
    return review;
  } catch (e) {
    return res.json({ status: "error", message: e.message });
  }
};

// service for get data using id
exports.show = async function (id, res) {
  try {
    const review = await Review.findById(id);
    return review;
  } catch (e) {
    return res.json({ status: "error", message: e.message });
  }
};
// service for post data
exports.create = async function (review, res) {
  try {
    const dat = await new Review(review);
    await dat.save();
    return dat;
  } catch (error) {
    return res.json({ status: "error", message: error.message });
  }
};
// service for update data using id
exports.update = async function (id, review, res) {
  try {
    const dat = await Review.findByIdAndUpdate(id, review, { new: true });
    return dat;
  } catch (error) {
    return res.json({ status: "error", message: error.message });
  }
};
// service for delete data using id
exports.delete = async function (id, res) {
  try {
    const dat = await Review.findByIdAndRemove(id);
    return dat;
  } catch (error) {
    return res.json({ status: "error", message: error.message });
  }
};
